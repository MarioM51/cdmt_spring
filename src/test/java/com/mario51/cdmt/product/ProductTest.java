package com.mario51.cdmt.product;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

//import javax.validation.ConstraintViolationException;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
//import com.mario51.cdmt.error.EError;
import com.mario51.cdmt.product.impl.ProductController;
import com.mario51.cdmt.product.model.ProductEntity;


@WebMvcTest(ProductController.class)
public class ProductTest {

  @InjectMocks
  private ProductController productController;

  @Autowired
  ObjectMapper objectMapper;
  
  @MockBean 
  private IProductService productService;

  @Autowired
  MockMvc mockMvc;
  
  
  @Test
  public void save_mustReturnSameValuesAndAddIdField() throws Exception {
    ProductEntity productEntered = new ProductEntity("Trompo pequeño", 10, 45.0, "Trompo pequeño para jugar");
    ProductEntity productExpected = new ProductEntity("Trompo pequeño", 10, 45.0, "Trompo pequeño para jugar");
    productExpected.setProductId(1);

    when(productService.save( any() )).thenReturn(productExpected);
    
    String productEnteredJson = objectMapper.writeValueAsString(productEntered);

    mockMvc
      .perform(post("/api/products/")
        .contentType(MediaType.APPLICATION_JSON)
        .content(productEnteredJson)
      )
      //respnse
        .andExpect(status().isCreated())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))

        .andExpect(jsonPath("$.productId", is(productExpected.getProductId())))
        .andExpect(jsonPath("$.name", is(productEntered.getName())))
        .andExpect(jsonPath("$.amount", is(productEntered.getAmount())))
        .andExpect(jsonPath("$.price", is(productEntered.getPrice())))
        .andExpect(jsonPath("$.description", is(productEntered.getDescription())))
        //.andExpect(jsonPath("$.status", is("NOT_PUBLISHED")))
    ;
  }

}
