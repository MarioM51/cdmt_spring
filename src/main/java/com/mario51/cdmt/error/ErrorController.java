package com.mario51.cdmt.error;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import com.mario51.cdmt.error.model.BadMethodCallException;
import com.mario51.cdmt.error.model.ErrorModel;
import com.mario51.cdmt.error.model.ErrorTypeEnum;
import com.mario51.cdmt.error.utils.DataIntegrityViolationHandler;
import com.mario51.cdmt.error.utils.HadleErrorUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ErrorController {

  final Logger logger = LoggerFactory.getLogger(ErrorController.class);

  @PostConstruct
  private void onPostConstruct() {
    logger.info("created ErrorController");
  }
  
  @ExceptionHandler(ConstraintViolationException.class)
  @ResponseBody
  public ErrorModel dataValidationConflic(ConstraintViolationException ex, HttpServletResponse response) {
    List<String> messagess = HadleErrorUtils.getErrorMessagess(ex);
    ErrorModel errorModel = new ErrorModel(ErrorTypeEnum.BAD_INPUT, messagess);
    return errorModel;
  }

  @ExceptionHandler(DataIntegrityViolationException.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ErrorModel dataIntegrityConflic(DataIntegrityViolationException ex) {
    final var dataIntVio = new DataIntegrityViolationHandler();
    final ErrorModel errorModel = dataIntVio.getErrorModel(ex);
    return errorModel;
  }

  @ExceptionHandler(BadMethodCallException.class)
  @ResponseBody
  public ErrorModel dataBadMethodCall(BadMethodCallException ex) {
    return ex;
  }

  @ExceptionHandler(ErrorModel.class)
  @ResponseBody
  public ErrorModel dataBadMethodCall(ErrorModel ex) {
    return ex;
  }

  
}
