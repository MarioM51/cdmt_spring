package com.mario51.cdmt.error.model;

import org.springframework.http.HttpStatus;

public enum ErrorTypeEnum {
  
  BAD_INPUT("Datos invalidos", HttpStatus.BAD_REQUEST),
  NOT_FOUND("Recurso no encontrado", HttpStatus.NOT_FOUND),
  INTERNAL_ERROR("Error, intente mas tarde y si persiste contacte al administrador", HttpStatus.INTERNAL_SERVER_ERROR)
  ;
  
  public String msg;

  public HttpStatus httpStatus;
  
  ErrorTypeEnum(String msg, HttpStatus httpStatus) {
    this.msg=msg;
    this.httpStatus=httpStatus;
  }

}
