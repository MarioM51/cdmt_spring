package com.mario51.cdmt.error.model;

public class BadMethodCallException extends ErrorModel {
  private static final long serialVersionUID = 1L;
  
  public BadMethodCallException() {
    super(ErrorTypeEnum.INTERNAL_ERROR);
    
  }

}
