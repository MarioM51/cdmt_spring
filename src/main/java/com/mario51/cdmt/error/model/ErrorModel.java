package com.mario51.cdmt.error.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.springframework.http.HttpStatus;

@JsonIgnoreProperties({"stackTrace", "status", "suppressed", "localizedMessage", "cause"})
public class ErrorModel extends RuntimeException {
  
  private static final long serialVersionUID = 3941264554869559259L;

  private List<String> details;

  private String message;

  private ErrorTypeEnum errorType;

  private HttpStatus status;

  public ErrorModel(ErrorTypeEnum errorType, List<String> details, HttpStatus status) {
    super(errorType.msg);
    this.message = errorType.msg;
    this.errorType = errorType;
    this.details = details;
    this.status = status;
  }

  public ErrorModel(ErrorTypeEnum errorType, List<String> details) {
    super(errorType.msg);
    this.message = errorType.msg;
    this.errorType = errorType;
    this.details = details;
    this.status = errorType.httpStatus;
  }

  public ErrorModel(ErrorTypeEnum errorType, String details) {
    super(errorType.msg);
    this.message = errorType.msg;
    this.errorType = errorType;
    this.status = errorType.httpStatus;
    this.details = Arrays.asList(details);
  }

  public ErrorModel(ErrorTypeEnum errorType) {
    super(errorType.msg);
    this.errorType = errorType;
    this.message = errorType.msg;
    this.status = errorType.httpStatus;
  }

  public List<String> getDetails() {
    return this.details;
  }

  public void setDetails(List<String> details) {
    this.details = details;
  }

  public String getMessage() {
    return this.message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getErrorType() {
    return this.errorType.name();
  }

  public void setErrorType(ErrorTypeEnum errorType) {
    this.errorType = errorType;
  }


  public HttpStatus getStatus() {
    return this.status;
  }

  public void setStatus(HttpStatus status) {
    this.status = status;
  }

}
