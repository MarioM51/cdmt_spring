package com.mario51.cdmt.error.utils;

import com.mario51.cdmt.error.model.BadMethodCallException;

public class MyAssert {
  static final public void assertThat(final boolean myAssert) {
    if(!myAssert) {
      throw new BadMethodCallException();
    }
  }
}
