package com.mario51.cdmt.error.utils;

import java.util.Map;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;

import com.mario51.cdmt.error.model.ErrorModel;
import com.mario51.cdmt.error.model.ErrorTypeEnum;

public class DataIntegrityViolationHandler {
  
  private static final Map<String, String> CONSTRAINS = Map.of(
    "product_name_unique_violation", "Ya existe un producto con ese nombre"
  );

  public ErrorModel getErrorModel(DataIntegrityViolationException ex) {
    String rootMsg = this.getConstraintName(ex);
    if (rootMsg != null) {
        String lowerCaseMsg = rootMsg.toLowerCase();
        for (Map.Entry<String, String> entry : CONSTRAINS.entrySet()) {
            if (lowerCaseMsg.contains(entry.getKey())) {
                return new ErrorModel(ErrorTypeEnum.BAD_INPUT, entry.getValue());
            }
        }
    }
    ex.printStackTrace();
    return new ErrorModel(ErrorTypeEnum.INTERNAL_ERROR, "DataIntegrityViolationException: " + rootMsg);
  }

  private String getConstraintName(DataIntegrityViolationException ex) {
    if(ex.getCause() instanceof ConstraintViolationException) {
      var ConstrainViotEx = (ConstraintViolationException) ex.getCause();
      return ConstrainViotEx.getConstraintName();
    }
    return null;
  }  

}
