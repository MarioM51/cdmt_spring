package com.mario51.cdmt.error.utils;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.ArrayList;
import java.util.List;

public class HadleErrorUtils {

	public static List<String> getErrorMessagess(ConstraintViolationException ex) {
    Set<ConstraintViolation<?>> constraintViolations = ex.getConstraintViolations();

    List<String> messages = new ArrayList<>(constraintViolations.size());
    messages.addAll(constraintViolations.stream()
        .map(constraintViolation -> String.format("field '%s' value '%s' %s", constraintViolation.getPropertyPath(),
                constraintViolation.getInvalidValue(), constraintViolation.getMessage()))
        .collect(Collectors.toList()));

		return messages;
	}
  
}
