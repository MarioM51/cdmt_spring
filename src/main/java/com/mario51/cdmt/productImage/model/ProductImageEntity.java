package com.mario51.cdmt.productImage.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mario51.cdmt.product.model.ProductEntity;

@Entity
@Table(
  name="product_images",
  uniqueConstraints = {
    @UniqueConstraint( columnNames = {"image_name"}, name = "product_image_image_name_unique_violation")
  }
)
@JsonIgnoreProperties({"product", "base64"})
public class ProductImageEntity {
  
  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Integer productImageId;
  
  @NotNull(message="El nombre es obligatorio")
  @Column(name = "image_name", length = 50, nullable = false)
  private String imageName;

  @NotNull(message="La imagen no tiene el tipo de contenido")
  @Column(length = 15, nullable = false)
  private String contentType;

  @Column(columnDefinition = "TEXT", nullable = false)
  private String base64;

  private Boolean mainImage;

  @ManyToOne(fetch=FetchType.LAZY)
  @JoinColumn(name="product_fk", nullable=false)
  @NotNull(message="El id de producto es obligarorio")
  //@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "name", "price", "amount", "description", "user"})
  private ProductEntity product;

  public ProductImageEntity() { }

  public ProductImageEntity(Integer productImageId, String imageName, String contentType, String base64, ProductEntity product) {
    this.productImageId = productImageId;
    this.imageName = imageName;
    this.contentType = contentType;
    this.base64 = base64;
    this.product = product;
  }

  public Integer getProductImageId() {
    return this.productImageId;
  }

  public void setProductImageId(Integer productImageId) {
    this.productImageId = productImageId;
  }

  public String getImageName() {
    return this.imageName;
  }

  public void setImageName(String imageName) {
    this.imageName = imageName;
  }

  public String getContentType() {
    return this.contentType;
  }

  public void setContentType(String contentType) {
    this.contentType = contentType;
  }

  public String getBase64() {
    return this.base64;
  }

  public void setBase64(String base64) {
    this.base64 = base64;
  }

  public ProductEntity getProduct() {
    return this.product;
  }

  public void setProduct(ProductEntity product) {
    this.product = product;
  }

  public Boolean isMainImage() {
    return this.mainImage;
  }

  public void setMainImage(Boolean mainImage) {
    this.mainImage = mainImage;
  }


}
