package com.mario51.cdmt.productImage;

import java.util.List;

import com.mario51.cdmt.product.model.ProductEntity;
import com.mario51.cdmt.productImage.model.ProductImageEntity;

import org.springframework.web.multipart.MultipartFile;

public interface IProductImageService {

  ProductImageEntity findProductMainImageByProductId(Integer id);

  ProductImageEntity save(Integer idProduct, MultipartFile image);

  List<ProductImageEntity> findAllImagesByProductId(Integer productId);

  ProductImageEntity findById(Integer imageId);

  List<ProductImageEntity> deleteImagesOfProduct(ProductEntity product);
  
  
}

