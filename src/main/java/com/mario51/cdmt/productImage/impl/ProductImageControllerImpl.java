package com.mario51.cdmt.productImage.impl;

import java.util.List;

import com.mario51.cdmt.productImage.IProductImageService;
import com.mario51.cdmt.productImage.model.ProductImageEntity;
import com.mario51.cdmt.utils.FileUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/products")
public class ProductImageControllerImpl {

  @Autowired
  private IProductImageService productImageService;

  @GetMapping("/{productId}/images")
  @ResponseStatus(value=HttpStatus.OK)
  List<ProductImageEntity> findAllImagesOfProduct(@PathVariable int productId) {
    return productImageService.findAllImagesByProductId(productId);
  }

  
  @PostMapping("/{productId}/images")
  @ResponseStatus(value=HttpStatus.CREATED)
  ProductImageEntity addImage(@PathVariable int productId, @RequestParam("image") MultipartFile image) {
    return productImageService.save(productId, image);
  }

  
  @GetMapping("/{productId}/images/main-image")
  @ResponseStatus(value=HttpStatus.OK)
  ResponseEntity<byte[]> getMainImageOfProduct(@PathVariable int productId) {
    ProductImageEntity mainProductImage = this.productImageService.findProductMainImageByProductId(productId);
    return FileUtils.getResponseEntity(mainProductImage);
  }

  @GetMapping("/images/{imageId}")
  @ResponseStatus(value=HttpStatus.OK)
  ResponseEntity<byte[]> getImageByProductImageId(@PathVariable int imageId) {
    ProductImageEntity productImageFinded = this.productImageService.findById(imageId);
    return FileUtils.getResponseEntity(productImageFinded);
  }
  
}
