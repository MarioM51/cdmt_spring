package com.mario51.cdmt.productImage.impl;

import java.util.ArrayList;
import java.util.List;

import com.mario51.cdmt.error.model.ErrorModel;
import com.mario51.cdmt.error.model.ErrorTypeEnum;
import com.mario51.cdmt.error.utils.MyAssert;
import com.mario51.cdmt.product.IProductService;
import com.mario51.cdmt.product.model.ProductEntity;
import com.mario51.cdmt.productImage.IProductImageDAO;
import com.mario51.cdmt.productImage.IProductImageService;
import com.mario51.cdmt.productImage.model.ProductImageEntity;
import com.mario51.cdmt.utils.FileUtils;
import com.mario51.cdmt.utils.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ProductImageServiceImpl implements IProductImageService {

  @Autowired
  private IProductService productService;

  @Autowired
  IProductImageDAO productImageDao;

  @Override
  public ProductImageEntity findProductMainImageByProductId(Integer productId) {
    MyAssert.assertThat(productId != null && productId >= 0 );

    ProductEntity product = productService.findById(productId);

    ProductImageEntity productImage = product.getImages().stream()
      .filter((p) -> p.isMainImage() == true)
      .findFirst().orElse(null);
    
    if(productImage == null) {
      throw new ErrorModel(ErrorTypeEnum.NOT_FOUND, String.format("El producto con Id %s no tiene una imagen principal", product.getProductId()));
    }

    return productImage;
  }

  @Override
  public ProductImageEntity save(Integer idProduct, MultipartFile productImageFile) {
    MyAssert.assertThat(idProduct != null);
    MyAssert.assertThat(productImageFile != null);
    
    ProductEntity product = productService.findById(idProduct);

    for (ProductImageEntity image : product.getImages()) {
      productImageDao.deleteById(image.getProductImageId());
    }

    ProductImageEntity newProductImage = new ProductImageEntity();
    newProductImage.setMainImage(true);
    newProductImage.setProduct(product);
    
    newProductImage.setImageName(buildImageName(product, productImageFile.getOriginalFilename()));
    newProductImage.setContentType(productImageFile.getContentType());
    newProductImage.setBase64(FileUtils.getBase64(productImageFile));

    return productImageDao.save(newProductImage);
  }

  private String buildImageName(ProductEntity product, String imageName) {
    String productNameClean = StringUtils.cleanToUrl(product.getName());
    String imageNum = product.getImages().size()+"";
    String extension = imageName.substring(imageName.lastIndexOf("."));
    return productNameClean + "-" + imageNum + extension;
  }

  @Override
  public List<ProductImageEntity> findAllImagesByProductId(Integer productId) {
    MyAssert.assertThat(productId != null && productId >= 0 );
    
    ProductEntity product = productService.findById(productId);

    if(product.getImages() != null && product.getImages().size() <= 0) {
      return new ArrayList<ProductImageEntity>();
    }

    return product.getImages();
  }

  @Override
  public ProductImageEntity findById(Integer imageId) {
    MyAssert.assertThat(imageId != null && imageId >= 0 );
    ProductImageEntity productImage = productImageDao.findById(imageId).orElse(null);
    if(productImage == null) {
      throw new ErrorModel( ErrorTypeEnum.NOT_FOUND, String.format("Imagen con id %s no encontrado", imageId));
    }
    return productImage;
  }

  @Override
  public List<ProductImageEntity> deleteImagesOfProduct(ProductEntity product) {
    for (ProductImageEntity productImage : product.getImages()) {
      productImageDao.deleteById(productImage.getProductImageId());
    }
    return product.getImages();
  }

  
  
}
