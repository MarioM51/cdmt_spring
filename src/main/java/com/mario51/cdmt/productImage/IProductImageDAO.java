package com.mario51.cdmt.productImage;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

import com.mario51.cdmt.productImage.model.ProductImageEntity;

public interface IProductImageDAO extends CrudRepository<ProductImageEntity, Integer> {
  
  @Query("select image from ProductImageEntity image where image.product.productId = ?1")
  List<ProductImageEntity> findByProductId(int idProduct);
  
}
