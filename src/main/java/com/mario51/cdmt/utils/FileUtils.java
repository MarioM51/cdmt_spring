package com.mario51.cdmt.utils;

import java.io.IOException;

import com.mario51.cdmt.error.model.ErrorModel;
import com.mario51.cdmt.error.model.ErrorTypeEnum;
import com.mario51.cdmt.productImage.model.ProductImageEntity;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.multipart.MultipartFile;

public class FileUtils {

  public static String getBase64(MultipartFile file) {
    try {
      byte[] bytes = file.getBytes();
      return new String(Base64Utils.encode(bytes));
    } catch (IOException e) {
      e.printStackTrace();
      throw new ErrorModel(ErrorTypeEnum.INTERNAL_ERROR, "Error al procesar la imagen (conversion a base64)");
    }

  }

  public static ResponseEntity<byte[]> getResponseEntity(ProductImageEntity file) {
    String base64 = file.getBase64();
    byte[] bytes = Base64.decodeBase64(base64);
    
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.IMAGE_PNG);
    
    return new ResponseEntity<byte[]> (bytes, headers, HttpStatus.OK);
  }
  
}
