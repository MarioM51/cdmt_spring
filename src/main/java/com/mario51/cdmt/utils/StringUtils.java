package com.mario51.cdmt.utils;

public class StringUtils {

  public static String cleanToUrl(String toClean) {
    String cleaned = toClean.replaceAll("(?![a-zA-Z1-9.,]).", "-").toLowerCase();
    return cleaned;
  }
  
}
