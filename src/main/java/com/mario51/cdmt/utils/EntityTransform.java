package com.mario51.cdmt.utils;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

/***
 * Fuente: https://stackoverflow.com/questions/27818334/jpa-update-only-specific-fields
 * 
 * Ejemplo:
public ResponseEntity<?> updateUser(@RequestBody User newUserInfoWithNulls) {

  User existing = userRepository.read(user.getId());
  EntityTransform.copyNonNullProperties(newUserInfoWithNulls, existing);
  userRepository.save(existing);

  // ...
}
*/

//no se intancia ya que todos sus metodos son estaticos
public class EntityTransform {

  private EntityTransform(){  }

  public static void copyNonNullProperties(Object src, Object target) {
    BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
  }

  private static String[] getNullPropertyNames (Object source) {
      final BeanWrapper src = new BeanWrapperImpl(source);
      java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

      Set<String> emptyNames = new HashSet<String>();
      for(java.beans.PropertyDescriptor pd : pds) {
          Object srcValue = src.getPropertyValue(pd.getName());
          if (srcValue == null) emptyNames.add(pd.getName());
      }
      String[] result = new String[emptyNames.size()];
      return emptyNames.toArray(result);
  }
  
}
