package com.mario51.cdmt.product.model;

public enum ProductStatusEnum {
  
  NOT_PUBLISHED,
  NOT_UPDATED,
  PUBLISHED
  ;
}
