package com.mario51.cdmt.product.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;

import com.mario51.cdmt.productImage.model.ProductImageEntity;

@Entity
@Table(
  name="products",
  uniqueConstraints = {
    @UniqueConstraint( columnNames = {"name"}, name = "product_name_unique_violation")
  }
)
public class ProductEntity {

  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Integer productId;

  @Size(min=3, max=60)
  @Column(name = "name")
  private String name;

  private Integer amount;

  private Double price;

  @Column(length=160)
  private String description;

  private String status;

  @Column(name="create_at")
  @Temporal(TemporalType.TIMESTAMP)
  private Date createAt;

  @Column(name="last_update")
  @Temporal(TemporalType.TIMESTAMP)
  private Date lastUpdate;

  //@NotNull(message="Al menos una imagen es obligatoria")
  @OneToMany(fetch=FetchType.LAZY)
  @JoinColumn(name = "product_fk")
  //@OnDelete(action = OnDeleteAction.NO_ACTION)
  private List<ProductImageEntity> images;

  @PrePersist
  public void prePersist() {
    this.createAt = new Date();
    this.lastUpdate = new Date();
    this.status = ProductStatusEnum.NOT_PUBLISHED.toString();
  }

  public ProductEntity() { }

  public ProductEntity(String name, Integer amount, Double price, String description) {
    this.name = name;
    this.amount = amount;
    this.price = price;
    this.description = description;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getAmount() {
    return this.amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

  public Double getPrice() {
    return this.price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public String getDescription() {
    return this.description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getStatus() {
    return this.status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Integer getProductId() {
    return this.productId;
  }

  public void setProductId(Integer productId) {
    this.productId = productId;
  }

  public Date getCreateAt() {
    return this.createAt;
  }

  public void setCreateAt(Date createAt) {
    this.createAt = createAt;
  }

  public Date getLastUpdate() {
    return this.lastUpdate;
  }

  public void setLastUpdate(Date lastUpdate) {
    this.lastUpdate = lastUpdate;
  }


  public List<ProductImageEntity> getImages() {
    return this.images;
  }

  public void setImages(List<ProductImageEntity> images) {
    this.images = images;
  }


}
