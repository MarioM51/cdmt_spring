package com.mario51.cdmt.product;

import org.springframework.data.repository.CrudRepository;

import com.mario51.cdmt.product.model.ProductEntity;

public interface IProductDAO extends CrudRepository<ProductEntity, Integer> {
  
}