package com.mario51.cdmt.product;

import java.util.List;

import com.mario51.cdmt.product.model.ProductEntity;

public interface IProductService {
  
  ProductEntity save(ProductEntity productEntity);

  List<ProductEntity> findAll();

  public ProductEntity findById(int productId);

  ProductEntity deleteById(Integer productId);

  ProductEntity update(ProductEntity newInfoProduct);
}
