package com.mario51.cdmt.product.impl;

import java.util.List;

import com.mario51.cdmt.error.model.ErrorModel;
import com.mario51.cdmt.error.model.ErrorTypeEnum;
import com.mario51.cdmt.error.utils.MyAssert;
import com.mario51.cdmt.product.IProductDAO;
import com.mario51.cdmt.product.IProductService;
import com.mario51.cdmt.product.model.ProductEntity;
import com.mario51.cdmt.productImage.IProductImageService;
import com.mario51.cdmt.utils.EntityTransform;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductServiceImpl implements IProductService {
  
  @Autowired
  private IProductDAO productDAO;

  @Autowired
  private IProductImageService productImageService;
  
  @Override
  @Transactional
  public ProductEntity save(ProductEntity productEntity) {
    MyAssert.assertThat(productEntity.getProductId() == null);
    
    ProductEntity saved = null;
    saved = this.productDAO.save(productEntity); // DataIntegrityViolationException
    return saved;
  }
  

  @Override
  @Transactional(readOnly = true)
  public List<ProductEntity> findAll() {
    List<ProductEntity> all = (List<ProductEntity>)this.productDAO.findAll();
    return all;
  }


  @Override
  @Transactional(readOnly = true)
  public ProductEntity findById(int productId) {
    ProductEntity productFinded = this.productDAO.findById(productId).orElse(null);
    if(productFinded==null) {
      throw new ErrorModel(ErrorTypeEnum.NOT_FOUND, String.format("El producto con Id %s no se encontro", productId));
    }
    return productFinded;
  }


  @Override
  public ProductEntity deleteById(Integer productId) {
    ProductEntity productToDelete = this.findById(productId);
    productImageService.deleteImagesOfProduct(productToDelete);
    this.productDAO.deleteById(productToDelete.getProductId());
    return productToDelete;
  }


  @Override
  public ProductEntity update(ProductEntity newInfoProduct) {
    MyAssert.assertThat(newInfoProduct.getProductId() != null);

    ProductEntity productFinded = this.findById(newInfoProduct.getProductId());
    if(productFinded == null) {
      return null;
    }

    //evitar sobre escribir con null
    EntityTransform.copyNonNullProperties(newInfoProduct, productFinded);

    ProductEntity productUpdated = this.productDAO.save(productFinded);
    return productUpdated;
  }
}
