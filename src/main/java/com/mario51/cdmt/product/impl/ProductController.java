package com.mario51.cdmt.product.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.validation.Valid;

import com.mario51.cdmt.product.IProductService;
import com.mario51.cdmt.product.model.ProductEntity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/products")
public class ProductController {

  final Logger logger = LoggerFactory.getLogger(ProductController.class);

  @Autowired
  IProductService productService;

  @PostConstruct
  private void onPostConstruct() {
    logger.info("created /api/products");
  }
  
  @PostMapping
  @ResponseStatus(value=HttpStatus.CREATED)
  public ProductEntity save(@Valid @RequestBody ProductEntity newProduct) {
    newProduct.setProductId(null);
    newProduct.setCreateAt(null);
    newProduct.setLastUpdate(null);
    return productService.save(newProduct);
  }

  @GetMapping
  @ResponseStatus(value=HttpStatus.OK)
  public List<ProductEntity> findAll() {
    return productService.findAll();
  }

  @GetMapping({"/{productId}"})
  @ResponseStatus(value=HttpStatus.OK)
  public ProductEntity findById(@PathVariable Integer productId) {
    return productService.findById(productId);
  }

  @DeleteMapping({"/{productId}"})
  @ResponseStatus(value=HttpStatus.ACCEPTED)
  public ProductEntity deleteById(@PathVariable Integer productId) {
    return productService.deleteById(productId);
  }

  @PutMapping
  @ResponseStatus(value=HttpStatus.ACCEPTED)
  public ProductEntity update(@Valid @RequestBody ProductEntity newInfoProduct) {
    return productService.update(newInfoProduct);
  }

}
